var http = require('http');
var url = require('url');
var util = require('util');
var queryString = require('querystring');

var postHTML = 
  '<html><head><meta charset="utf-8"><title>菜鸟教程 Node.js 实例</title></head>' +
  '<body>' +
  '<form method="post">' +
  '网站名： <input name="name"><br>' +
  '网站 URL： <input name="url"><br>' +
  '<input type="submit">' +
  '</form>' +
  '</body></html>';

function start(route) {
    function onRequest(request, response) {
        var body = '';
        request.on('data', function (chuck) {
            body += chuck;
            console.log('body = ', body);
        })
        request.on('end', function () {
            // 解析参数
            body = queryString.parse(body);
            // 设置响应头部信息及编码
            response.writeHead(200, {'Content-Type': 'text/html;charset=utf8'});

            if (body.name && body.url) {
                console.log('body.name && body.url');
                // http://localhost:8888/user?name=richard&url=www.runoob.com
                response.write('网站名：' + body.name);
                response.write('\n');
                response.write('网站URL：' + body.url);
            } else {
                response.write(postHTML);
            }
            response.end();
        })
    }
    http.createServer(onRequest).listen(8888);
    console.log("Server has started.");
}

exports.start = start;