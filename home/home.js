
var fs = require('fs');

/**
 * 回调函数的说明
 */
// 阻塞
if (false) {
    var data = fs.readFileSync('home/input.txt');
    console.log(data.toString());
    console.log("阻塞 程序执行结束!");
    // 非阻塞 使用了回调函数
    fs.readFile('home/input.txt', function (err, data) {
        if (err) return console.error(err);
        console.log(data.toString());
    });
    console.log("非阻塞 程序执行结束!");
}

/** 
 * 事件驱动模型
 * 触发connection --> connectHandler中触发data_received --> 数据接收成功
 * 可以重复触发事件
 * 一次可以触发多个事件 按顺序依次调用
 * 事件中参数多 而触发时候参数少 多余的参数为underfined
*/
if (false) {
    var events = require('events');
    var eventEmitter = new events.EventEmitter(); // EventEmitter对象
    // 创建事件处理程序
    var connectHandler = function connected() {
        console.log('连接成功。');
        eventEmitter.emit('data_received'); // 触发 data_received 事件 
     }
      //add listener 绑定监听器的尾部
      eventEmitter.addListener('connection', connectHandler);
      eventEmitter.on('connection', connectHandler);  // 绑定 connection 事件处理程序
     // 使用匿名函数绑定 data_received 事件
     eventEmitter.on('data_received', function(){
        console.log('数据接收成功。');
     });
     console.log('开始连接 connection');
     eventEmitter.emit('connection');  // 触发 connection 事件 

     

     // 2秒后触发事件
     eventEmitter.on('circle_event', function () {
         console.log('触发延后事件 circle_event');
     });
     setTimeout(function () {
         eventEmitter.emit('circle_event');
     }, 2000);
    
     // 多参数事件 一次可以触发多个事件 按顺序依次调用
     eventEmitter.on('more_args_event', function (arg1, arg2, arg3) {
         console.log('监听者1号 ---- ', arg1, arg2, arg3);
     })
     eventEmitter.on('more_args_event', function (arg1, arg2) {
        console.log('监听者2号 ---- ', arg1, arg2);
    })
    eventEmitter.emit('more_args_event', 'arg1', 'arg2', 'arg3');
    eventEmitter.emit('more_args_event', '参数1'); // 如果参数少 之后的为underfined
    
    

    var eventEmitter_connection_count = eventEmitter.listenerCount('connection'); // 获取指定名字的事件个数
    console.log(eventEmitter_connection_count);
    eventEmitter.removeListener('connection', connectHandler); // 移除指定名字的事件，此操作将会改变处于被删监听器之后的那些监听器的索引
    
    eventEmitter.on('error',function(err){
        console.error('Error:',err);
    });
    eventEmitter.emit('error')
    

     console.log("程序执行完毕。");
}

/** 
 * Buffer 二进制缓冲区
 * ascii   -   仅支持 7 位 ASCII 数据。如果设置去掉高位的话，这种编码是非常快的。
 * utf8    -   多字节编码的 Unicode 字符。许多网页和其他文档格式都使用 UTF-8 。
 * utf16le -   2 或 4 个字节，小字节序编码的 Unicode 字符。支持代理对（U+10000 至 U+10FFFF）。
 * ucs2    -   utf16le 的别名
 * base64  -   Base64 编码。
 * latin1  -   一种把 Buffer 编码成一字节编码的字符串的方式。
 * binary  -   latin1 的别名。
 * hex     -   将每个字节编码为两个十六进制字符。
*/
if (false) {
    const buf = Buffer.from('runoob', 'ascii');
    // 输出 72756e6f6f62
    console.log(buf.toString('hex'));
    // 输出 cnVub29i
    console.log(buf.toString('base64'));


    /** --创建Buffer-- */
    // Buffer.alloc(size[, fill[, encoding]]);创建一个长度为 10 默认不填为0 填1则Buffer被填充
    const buf1 = Buffer.alloc(10);
    const buf2 = Buffer.alloc(10, 1);
    
    // Buffer.allocUnsafe(size);速度比alloc()快 但可能是被污染的buf 需要fill()或write()重写；创建一个长度为 10
    // Buffer.allocUnsafeSlow(size);慢动作的吗？？
    const buf3 = Buffer.allocUnsafe(10);

    // Buffer.from(array);数组里面只能是数字，不然变为0
    const buf4 = Buffer.from([1, 2, 3, 16, '2222']);

    // Buffer.from(string[, encoding]); 返回UTF-8的buffer 可以指定格式默认是utf8，可以比如是latin1（Latin-1）
    const buf5 = Buffer.from('test');
    const buf6 = Buffer.from('tést', 'latin1');

    /** 
     * 写入缓冲区
     * buf.write(string[, offset[, length]][, encoding])
     * 如果buf空间不够 那么写入其中的一部分
     * offset 缓冲区开始写入的索引值 默认0 
     * length 写入的长度 默认buffer.length
     * encoding 编码方式 默认utf8
     */
    var buf_write = Buffer.alloc(256);
    len = buf_write.write("www.runoob.com");

    console.log("写入字节数 : "+  len); // 14

    /**
     * 从缓冲区读取数据
     * buf.toString([encoding[, start[, end]]])
     * encoding 编码方式 默认utf8
     * start 指定开始读取的索引位置 默认0
     * end 结束位置 默认末尾
     */
    var buf_read = Buffer.alloc(26);
    for (var i = 0 ; i < 26 ; i++) {
      buf_read[i] = i + 97;
    }
    console.log( buf_read.toString('ascii'));       // 输出: abcdefghijklmnopqrstuvwxyz
    console.log( buf_read.toString('ascii',0,5));   // 输出: abcde
    console.log( buf_read.toString('utf8',0,5));    // 输出: abcde
    console.log( buf_read.toString(undefined,0,5)); // 使用 'utf8' 编码, 并输出: abcde

    /**
     * 转JSON
     * buf.toJSON()
     * 当字符化buf的时候 JSON.stringify()会隐式地调用该toJSON()
     */
    const buf_json = Buffer.from([0x1, 0x2, 0x3, 0x4, 0x5]);
    const json = JSON.stringify(buf_json);

    // 输出: {"type":"Buffer","data":[1,2,3,4,5]}
    console.log(json);

    const copy = JSON.parse(json, (key, value) => {
        return value && value.type === 'Buffer' ? Buffer.from(value.data) : value;
    });

    // 输出: <Buffer 01 02 03 04 05>
    console.log(copy);

    /**
     * 缓冲区合并
     * Buffer.concat(list[, totalLength])
     * list 用来合并的Buffer数组
     * totalLength 合并后的指定长度
     */
    var buffer1 = Buffer.from(('菜鸟教程'));
    var buffer2 = Buffer.from(('www.runoob.com'));
    var buffer3 = Buffer.concat([buffer1,buffer2]);
    console.log("buffer3 内容: " + buffer3.toString());

    /**
     * 缓冲区比较
     * buf.compare(otherBuffer);
     */
    var buffer1 = Buffer.from('ABCE');
    var buffer2 = Buffer.from('ABCDF');
    var result = buffer1.compare(buffer2);

    if(result < 0) {
        console.log(buffer1 + " 在 " + buffer2 + "之前");
    }else if(result == 0){
        console.log(buffer1 + " 与 " + buffer2 + "相同");
    }else {
        console.log(buffer1 + " 在 " + buffer2 + "之后");
    }

    /**
     * 拷贝缓冲区
     * buf.copy(targetBuffer[, targetStart[, sourceStart[, sourceEnd]]])
     * targetBuffer 要拷贝的buffer对象
     * targetStart  数字 可选 默认0
     * sourceStart 数字 可选 默认0
     * sourceEnd 数字 可选 默认buffer.length
     */
    var buf_copy1 = Buffer.from('abcdefghijkl');
    var buf_copy2 = Buffer.from('RUNOOB');

    //将 buf2 插入到 buf1 指定位置上
    buf_copy2.copy(buf_copy1, 2);

    console.log(buf_copy1.toString()); // abRUNOOBijkl

    /**
     * 缓冲区剪裁
     * buf.slice([start[, end]])
     * start 数字 可选 默认0
     * end 数字 可选 默认buffer.length
     */
    var buffer1 = Buffer.from('runoob');
    // 剪切缓冲区
    var buffer2 = buffer1.slice(1,4);
    console.log("buffer2 content: " + buffer2.toString()); // uno
}

/**
 * stream
 * 四种类型：Readable可读 Writable可写 Duplex可读可写 Transform操作被写入数据然后读取
 * 所有的Stream对象都是EventEmitter的实例；
 * 常用的事件有：data当有数据时触发 end当没有更多的数据时触发 error发生错误触发 finish所有数据都写入到底层系统触发
 */
if (false) {
    var fs = require('fs');
    /**
     * 读取stream
     */
    var data = '';
    var readerStream = fs.createReadStream('home/input.txt');
    readerStream.setEncoding('UTF8');

    // stream事件
    readerStream.on('data', function(chunk) {
        data += chunk;
     });
     
     readerStream.on('end',function(){
        console.log('data = ', data);
     });
     
     readerStream.on('error', function(err){
        console.log('error = ', err.stack);
     });

     /**
      * 写入stream
      */
     var data = '两只老虎 两只老虎 跑得快 跑得快 一直没有眼睛 一直没有耳朵 真奇怪 真奇怪';
     var writerStream = fs.createWriteStream('home/output.txt');
     writerStream.write(data, 'UTF8');
     writerStream.end();
     // 处理流事件 --> data, end, and error
     writerStream.on('finish', function() {
        console.log("写入完成。");
     });

     writerStream.on('error', function(err){
        console.log(err.stack);
     });

     /**
      * 管道流pipe 类比于文件的复制
      */
     var readerStream = fs.createReadStream('home/output.txt');
     var writerStream = fs.createWriteStream('home/output2.txt');
     readerStream.pipe(writerStream);

     /**
      * 链式流
      * 通过连接输出流到另外一个流并创建多个流操作链的机制
      * 一般用于管道操作
      */
     // 压缩和解压
     var zlib = require('zlib');
     fs.createReadStream('home/input.txt')
        .pipe(zlib.createGzip())
        .pipe(fs.createWriteStream('home/input.txt.gz'));
    console.log("文件压缩完成。");
    
    fs.createReadStream('home/input.txt.gz')
        .pipe(zlib.createGunzip())
        .pipe(fs.createWriteStream('home/input_gz.txt'));
    console.log("文件解压完成。");
}

/**
 * 模块化
 */
if (false) {
    // exports.world = function(){}
    var hello_world = require('./helloworld');

    // module.exports = Hello
    hello_world.world();
    var Hello = require('./hello'); 
    hello = new Hello(); 
    hello.setName('小平平'); 
    hello.sayHello();
}

/**
 * 函数
 */
if (false) {
    /**
     * 匿名函数
     */
    function execute (someFunction, value) {
        someFunction(value);
    }

    execute(function(world) { console.log(world)}, '--==--hello--==--')
}

/**
 * 全局对象
 * 全局对象是 global，所有全局变量（除了 global 本身以外）都是 global 对象的属性。
 * 永远使用 var 定义变量以避免引入全局变量，因为全局变量会污染 命名空间，提高代码的耦合风险。
 */
if (false) {
    /**
     * __filename 
     * 表示当前正在执行的脚本的文件名,输出文件所在位置的绝对路径
     */
    console.log( __filename );  // /Users/daimengren/nodejstest/home/home.js

    /**
     * __dirname
     * 表示当前执行脚本所在的目录
     */
    console.log( __dirname ); // /Users/daimengren/nodejstest/home

    /**
     * setTimeout(cb, ms)
     * 只执行一次指定函数
     */
    setTimeout(() => {
        console.log('setTimeout 只执行一次指定函数');
    }, 2000);

    /**
     * clearTimeout(t) 清除指定定时器
     */
     var t = setTimeout(() => {
         console.log('不会打印的clearTimeout');
     }, 3000);
     clearTimeout(t);

     /**
      * setInterval(cb, ms)
      * 不停的调用直到清除
      */
     var t2 = setInterval(function () {
         console.log('不停的调用');
     }, 1000);
     setTimeout(() => {
         // 4s后清除
         clearTimeout(t2);
     }, 8000);
}

/**
 * 常用工具
 * util
 */
if (false) {
    var util = require('util');
    /**
     * util.inherits
     * sub仅仅继承prototype中的函数，并不继承在base中构造函数
     */
    function Base() {
        this.name = 'Base';
        this.base = 1991;
        this.sayHello = function () {
            console.log('hello' + this.name);
        }
    }
    Base.prototype.showName = function () {
        console.log(this.name);
    }
    function Sub () {
        this.name = 'sub';
    }
    util.inherits(Sub, Base);
    var base = new Base();
    base.showName();
    base.sayHello();
    console.log(base);
    var sub = new Sub();
    sub.showName();
    // sub.sayHello(); 并不继承这一项内容
    console.log(sub);
    
    /**
     * util.inspect(object,[showHidden],[depth],[colors])是一个将任意对象转换为字符串的方法
     * 通常用于调试和错误输出
     * showHidden 可选 为true时候输出更多隐藏信息
     * depth 可选 默认2 表示最大递归数，如果对象复杂可以指定输入的层数 为null时不限递归层数
     * colors 可选 true时以ANSI颜色输出
     */
    function Persion () {
        this.name = 'richard';
        this.toString = function () {
            return this.name;
        }
    }
    var persion = new Persion();
    console.log(util.inspect(persion));
    console.log(util.inspect(persion, true, null, true));

    /**
     * util.isArray(object)
     * 如果给定的参数 "object" 是一个数组返回true，否则返回false。
     */
    console.log('------  util.isArray()  ------');
    console.log(util.isArray([]));
    console.log(util.isArray('1111'));
    console.log(util.isArray({}));

    /**
     * util.isRegExp(object)
     * 如果给定的参数 "object" 是一个正则表达式返回true，否则返回false。
     */
    console.log('------  util.isRegExp()  -------');
    console.log(util.isRegExp(/some regexp/));
    console.log(util.isRegExp(new RegExp('another regexp')));
    console.log(util.isRegExp([]));

    /**
     * util.isDate(object)
     * 如果给定的参数 "object" 是一个日期返回true，否则返回false。
     */
    console.log('------  util.isDate()  ------');
    console.log(util.isDate(new Date()));
    console.log(util.isDate(Date()));
    console.log(util.isDate({}));
}

/**
 * 文件系统fs
 * 
 */
if (false) {
    var fs = require('fs');
    /**
     * 文件的同步读取和异步读取
     */
    // 异步
    fs.readFile('home/output.txt', function (err, data) {
        if (err) {
            return console.error(err);
        }
        console.log('fs 异步读取 ' + data.toString());
    })
    // 同步
    var read_sync_data = fs.readFileSync('home/output.txt');
    console.log('fs 同步读取 ' + read_sync_data.toString());

    /**
     * 打开文件
     * fs.open(path, flags[, mode], callback)
     * flags 打开文件的方式：
     *   r 读取方式；不存在抛出异常
     *   r+ 读写方式；不存在抛出异常
     *   rs 同步读取方式
     *   rs+ 同步读写方式
     *   w 写入方式；文件不存在则创建
     *   wx 写入方式；文件如果路径存在则写入失败
     *   w+ 读写方式；文件不存在则创建
     *   wx+ 读写方式；文件如果路径存在则写入失败
     *   a 追加方式；文件不存在则创建
     *   ax 追加方式；文件如果路径存在则追加失败
     *   a+ 读取追加方式；文件不存在则创建
     *   ax+ 读取追加方式；文件如果路径存在则追加失败
     * mode 设置文件权限 默认0666可读可写
     * callback 回调 如callback(err, fd)
     */
    // r+方式打开文件
    fs.open('home/input.txt', 'r+', function (err, fd) {
        if (err) {
            return console.error(err);
        }
        console.log('r+ 文件打开成功');
    })

    /**
     * 获取文件信息
     * fs.stat(path, callback) 异步
     * callback callback(err, stats)
     *   stats中包含：
     *   .isFile() 
     *   .isDirectory()
     *   .isBlockDevice()  块设备
     *   .isCharacterDevice() 字符设备
     *   .isSymbolicLink() 软链接
     *   isFIFO()
     *   isSocket()
     */
    fs.stat('home/input.txt', function (err, stats) {
        if (err) {
            return console.error(err);
        }
        console.log(stats);
        console.log('文件信息读取成功');
        console.log('isFile ' + stats.isFile());
        console.log('isDirectory ' + stats.isDirectory());
    })

    /**
     * 写入文件
     * fs.writeFile(file, data[, options], callback)
     * data 可以String或者Buffer
     * options {encoding, mode, flag} encoding默认utf8 mode默认0666 flag默认w
     * callback callback(err)
     */
    console.log('准备写入文件');
    fs.writeFile('home/input_write.txt', 'fs.writeFile 写入文件内容', function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('写入成功 读取写入的数据');
        fs.readFile('home/input_write.txt', function (err, data) {
            if (err) {
                return console.error(err);
            }
            console.log('data = ', data.toString());
        })
    })

    /**
     * 读取文件
     * fs.read(fd, buffer, offset, length, position, callback)
     * fd 通过fs.open()返回的文件秒数符
     * buffer 数据写入的缓冲区
     * offset 缓冲区的偏移量
     * length 要从文件中读取的字节数
     * position 起始位置 null时候从当前文件指针的位置读取
     * callback callback(err, bytesRead, buffer) 错误信息 读取的字节数 缓冲区对象
     */

     /**
     * 关闭文件
     * fs.close(fd, callback)
     * fd 通过fs.open()返回的文件秒数符
     * callback 没参数的回调函数
     */

    var buf = new Buffer.alloc(1024);
    fs.open('home/input_write.txt', 'r+', function (err, fd) {
        if (err) {
            return console.error(err);
        }
        console.log('文件打开成功 准备读取文件');
        fs.read(fd, buf, 0, buf.length, 0 ,function (err, bytes, buffer) {
            if (err) {
                return console.error(err);
            }
            console.log(bytes + ' 字节被读取');
            console.log(buffer);
            if (bytes > 0) {
                console.log('文件读取内容 = ', buf.slice(0, bytes).toString());
            }

            // 关闭文件
            fs.close(fd, function (err) {
                if (err) {
                    return console.error(err);
                }
                console.log('文件关闭成功');
            })
        })
    })
    
    /**
     * 截取文件
     * fs.ftruncate(fd, len, callback) 异步模式下
     * fd 通过fs.open()返回的文件秒数符
     * len 文件内容截取的长度
     * callback 没参数的回调函数
     */
    var buf = new Buffer.alloc(1024);
    fs.open('home/input_write.txt', 'r+', function (err, fd) {
        if (err) {
            return console.error(err);
        }
        fs.ftruncate(fd, 10, function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('截取成功');
            fs.read(fd, buf, 0, buf.length, 0, function (err, bytes) {
                if (err) {
                    return console.error(err);
                }
                if (bytes > 0) {
                    console.log('截取的部分data = ', buf.slice(0, bytes).toString());
                }
                fs.close(fd, function (err) {
                    if (err) {
                        return console.error(err);
                    }
                })
             })
        })
    })

    /**
     * 删除文件
     * fs.unlink(path, callback)
     */
    console.log('准备删除文件');
    fs.unlink('home/input.txt', function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('文件删除成功');
    })













}
