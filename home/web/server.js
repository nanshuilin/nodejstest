/**
 * HTTP服务器架构
 */
var http = require('http');
var fs = require('fs');
var url = require('url');

var pre_path = 'home/web';


var server = function (request, response) {
    // 解析请求，包括文件名
    var pathname = url.parse(request.url).pathname;
    console.log('输入请求的文件名  = ', pathname);
    // 从文件系统中读取请求的文件内容
    var path = pre_path + pathname
    console.log('read file = ', path);
    fs.readFile(path, function (err, data) {
        if (err) {
            console.log(err);
            // HTTP状态码404 NOT FOUND
            response.writeHead(404, {'Content-Type': 'text/html'});
        } else {
            // HTTP状态码200 OK
            response.writeHead(200, {'Content-Type': 'text/html'});
            // 响应文件内容
            response.write(data.toString());
        }
        // 发送响应数据
        response.end()
    });
}

// 创建服务器
http.createServer(server).listen(8080);

console.log('server running at http://127.0.0.1:8080/')