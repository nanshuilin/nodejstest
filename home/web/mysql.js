var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '123456',
    database: 'test'
})

connection.connect();

connection.query('SELECT 1 + 1 As solution', function (err, results, fields) {
    if (err) console.error(err);
    console.log(results);
    console.log('solution is : ', results[0].solution);
})