var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://127.0.0.1:27017/linnil';

MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
    if (err) {
        console.error(err)
        return;
    }
    // create_collection(db); // 创建集合site
    // insert(db); // 插入一条数据
    // insert_many(db); // 插入多条数据
    // find(db); // 查询数据
    // update(db); // 更新数据
    // updateMany(db); // 更新多条数据
    // deleteOne(db); // 删除数据
    // deleteMany(db); // 删除多条数据
    // sort(db); // 排序
    // limit_skip(db); // 分页读取
    // db.close();
})

// 创建集合
function create_collection (db) {
    var dbase = db.db('linnil');
    dbase.createCollection('site', function (err, res) {
        if (err) {
            console.error(err);
            return;
        }
        console.log('创建集合 site 成功');
        db.close()
    })
}

// 插入一条数据
function insert(db) {
    var dbase = db.db('linnil');
    var obj = {
        'name': '菜鸟网络',
        'url': 'www.runoob'
    }
    dbase.collection('site').insertOne(obj, function (err, res) {
        if (err) throw err;
        console.log('插入一条数据成功');
    })
    db.close();
}

// 插入多条数据
function insert_many(db) {
    var dbase = db.db('linnil');
    var obj = [
        {'name': '升序降序1', 'index': '1'},
        {'name': '升序降序2', 'index': '2'},
        {'name': '升序降序3', 'index': '3'},
        {'name': '升序降序4', 'index': '4'},
        {'name': '升序降序5', 'index': '5'}
    ]
    dbase.collection('site').insertMany(obj, function (err, res) {
        if (err) throw err;
        console.log('插入多条数据成功');
        db.close();
    })
}

// 查询数据
function find(db) {
    var dbase = db.db('linnil');
    // 指定查询条件
    var where = {'name': '菜鸟网络'};
    dbase.collection('site').find({}).toArray(function (err, result) {
        if (err) throw err;
        console.log('查询数据 result = ', result);
        db.close();
    })
    
}

// 更新数据
function update(db) {
    var dbase = db.db('linnil');
    var whereStr = {'name':'菜鸟网络'};
    var updateStr = {$set: {'url': 'www.test.com'}};
    dbase.collection('site').updateOne(whereStr, updateStr, function (err, res){
        if (err) throw err;
        console.log('更新数据成功');
        db.close();
    })
}

// 更新多条数据
function updateMany(db) {
    var dbase = db.db('linnil');
    var whereStr = {'type': 'update'};
    var updateStr = {$set: {'name': '已经是更新过的数据了'}}
    dbase.collection('site').updateMany(whereStr, updateStr, function (err, res) {
        if (err) throw err;
        console.log('更新多条数据成功');
        db.close();
    })
}

// 删除数据
function deleteOne(db) {
    var dbase = db.db('linnil');
    var whereStr = {'name': '菜鸟网络'};
    dbase.collection('site').deleteOne(whereStr, function (err, res) {
        if (err) throw err;
        console.log('删除成功');
        db.close();
    })
}

// 删除多条数据
function deleteMany(db) {
    var dbase = db.db('linnil');
    var whereStr = {'type': 'update'};
    dbase.collection('site').deleteMany(whereStr, function (err, res) {
        if (err) throw err;
        console.log('删除多条数据成功');
        db.close();
    })
}

// 排序
function sort(db) {
    var dbase = db.db('linnil');
    var sortCondition = {index: 1} // 1 升序 -1 降序
    dbase.collection('site').find().sort(sortCondition).toArray(function (err, res) {
        if (err) throw err;
        console.log('排序成功 res = ', res);
        db.close();
    })
}

// 分页查询
function limit_skip(db) {
    var dbase = db.db('linnil');
    var limit = 2;
    var skip = 2;
    // 限制读取
    dbase.collection('site').find().limit(limit).toArray(function (err, res) {
        if (err) throw err;
        console.log('读取两条信息成功 res = ', res);
    })
    dbase.collection('site').find().skip(skip).limit(limit).toArray(function (err, res) {
        if (err) throw err;
        console.log('跳过两条信息 res = ', res);
    })
}