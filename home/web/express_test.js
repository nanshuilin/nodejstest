var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var fs = require('fs');
var cookieParser = require('cookie-parser');
var util = require('util');

/**
 * 创建 application/x-www-form-urlencoded 编码解析
 */
var urlencodedParser = bodyParser.urlencoded({ extended: false })

/**
 * 静态文件
 * http://localhost:8081/images/logo.png 访问了静态文件public/images/logo.png
 */
app.use(express.static('public'));

/**
 * 上传文件
 */
app.use(multer({ dest: '/tmp/' }).array('image'));

/**
 * Cookie
 */
app.use(cookieParser());


/**
 * Request对象
 * req.app：当callback为外部文件时，用req.app访问express的实例
 * req.baseUrl：获取路由当前安装的URL路径
 * req.body / req.cookies：获得「请求主体」/ Cookies
 * req.fresh / req.stale：判断请求是否还「新鲜」
 * req.hostname / req.ip：获取主机名和IP地址
 * req.originalUrl：获取原始请求URL
 * req.params：获取路由的parameters
 * req.path：获取请求路径
 * req.protocol：获取协议类型
 * req.query：获取URL的查询参数串
 * req.route：获取当前匹配的路由
 * req.subdomains：获取子域名
 * req.accepts()：检查可接受的请求的文档类型
 * req.acceptsCharsets / req.acceptsEncodings / req.acceptsLanguages：返回指定字符集的第一个可接受字符编码
 * req.get()：获取指定的HTTP请求头
 * req.is()：判断请求头Content-Type的MIME类型
 * 
 * Response对象
 * res.app：同req.app一样
 * res.append()：追加指定HTTP头
 * res.set()在res.append()后将重置之前设置的头
 * res.cookie(name，value [，option])：设置Cookie 
 *      opition: domain / expires / httpOnly / maxAge / path / secure / signed
 * res.clearCookie()：清除Cookie
 * res.download()：传送指定路径的文件
 * res.get()：返回指定的HTTP头
 * res.json()：传送JSON响应
 * res.jsonp()：传送JSONP响应
 * res.location()：只设置响应的Location HTTP头，不设置状态码或者close response
 * res.redirect()：设置响应的Location HTTP头，并且设置状态码302
 * res.render(view,[locals],callback)：渲染一个view，同时向callback传递渲染后的字符串，如果在渲染过程中有错误发生next(err)将会被自动调用。callback将会被传入一个可能发生的错误以及渲染后的页面，这样就不会自动输出了。
 * res.send()：传送HTTP响应
 * res.sendFile(path [，options] [，fn])：传送指定路径的文件 -会自动根据文件extension设定Content-Type
 * res.set()：设置HTTP头，传入object可以一次设置多个头
 * res.status()：设置HTTP状态码
 * res.type()：设置Content-Type的MIME类型
 */

 // .all 全部中间件
app.all('/secret', function (req, res, next) {
    console.log('Accessing the secret section ...');
    next();
})

app.get('/index.htm', function (req, res) {
    res.sendFile(__dirname + '/' + 'index.htm');
})

app.get('/index/post.htm', function (req, res) {
    res.sendFile(__dirname + '/' + 'index_post.htm');
})

app.get('/index/upload.htm', function (req, res) {
    res.sendFile(__dirname + '/' + 'index_upload.htm');
})

// 响应 form action process_get
app.get('/process_get', function (req, res) {
    // 输出 JSON 格式
    var response = {
        'first_name': req.query.first_name,
        'last_name': req.query.last_name
    }
    console.log('response = ', response);
    res.end(JSON.stringify(response));
})

// 响应 form action process_post
app.post('/process_post', urlencodedParser, function (req, res) {
    // 输出 JSON 格式
    var response = {
        'first_name': req.body.first_name,
        'last_name': req.body.last_name
    }
    console.log('response = ', response);
    res.end(JSON.stringify(response));
})

// 响应 form action file_upload
app.post('/file_upload', function (req, res) {
    console.log(req.files[0]); // 上传的文件信息

    var des_file = __dirname + '/' + req.files[0].originalname;
    console.log('des_file = ', des_file);
    fs.readFile(req.files[0].path, function (err, data) {
        fs.writeFile(des_file, data, function (err) {
            if (err) {
                console.log(err);
            } else {
                response = {
                    message: 'File uploaded successfully',
                    filename: req.files[0].originalname
                }
            }
            console.log(response);
            res.end(JSON.stringify(response));
        })
    })
})

app.get('/', function (req, res) {
    console.log('cookies = ', util.inspect(req.cookies));
    res.send('主页 get请求');
})

app.post('/', function (req, res) {
    console.log('cookies = ', util.inspect(req.cookies));
    res.send('主页 post请求');
})

app.get('/del_user', function (req, res) {
    res.send('/del_user 响应 DELETE 请求');
})

// 显示所有的用户
app.get('/listUsers', function (req, res) {
    fs.readFile(__dirname + '/' + 'user.json', 'utf-8', function (err, data) {
        res.send(data);
    })
})

// 添加一个用户
var user = {
    "user4" : {
        "name" : "mohit",
        "password" : "password4",
        "profession" : "teacher",
        "id": 4
     }
}
app.get('/addUsers', function (req, res) {
    fs.readFile(__dirname + '/' + 'user.json', 'utf-8', function (err, data) {
        data = JSON.parse(data);
        data['user4'] = user['user4'];
        res.send(JSON.stringify(data));
    })
})

// 读取用户id
app.get('/user/:id', function (req, res) {
    fs.readFile(__dirname + '/' + 'user.json', 'utf-8', function (err, data) {
        data = JSON.parse(data);
        var user = data['user' + req.params.id];
        console.log('user = ', user);
        if (user) {
            res.send(JSON.stringify(user));
        } else {
            res.send(JSON.stringify('没有这个用户'));
        }
        
    })
})

// 删除用户
app.get('/deleteUsers/:id', function (req, res) {
    fs.readFile(__dirname + '/' + 'user.json', 'utf-8', function (err, data) {
        data = JSON.parse(data);
        var user = data['user' + req.params.id];
        if (user) {
            delete data['user' + req.params.id];
            res.send(JSON.stringify('删除成功'));
        } else {
            res.send(JSON.stringify('没有这个用户'));
        }
    })
})

app.get('/ab*cd', function (req, res) {
    res.send('正则匹配对页面 abcd, abxcd, ab123cd, 等响应 GET 请求');
})

app.get('/example/a', function (req, res) {
    res.send('example a');
})

app.get('/example/b', function (req, res, next) {
    console.log('will next');
    next();
}, function (req, res) {
    console.log('example b');
    res.send('example b');
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log('访问地址 http://%s:%s', host, port);
})